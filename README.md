
# 仓库说明

收集一些常用的环境的 docker-compose

## 环境依赖

1. docker
2. docker-compose

> 上面这两个环境为必须。没有安装的自行官网安装（可以使用国内源加速安装过程, 推荐使用 [Daolaod](http://get.daocloud.io/)）

## 快速开始

cd 到目标路径 使用docker compose 启动服务即可
 
## docker-compose 常用指令

1. 在后台启动服务
`docker-compose up -d`
2.  查看启动的服务
`docker-compose ps `
3.  列出指定服务的容器
`docker-compose ps eureka`
4.  停止服务
`docker-compose stop`
5.  启动已经存在的服务容器
`docker-compose start`
6.  重启项目中的服务
`docker-compose restart`
7. 停用移除所有容器以及网络相关
`docker-compose down`
8. 删除所有（停止状态的）服务容器
`docker-compose rm`
9. 查看日志
`docker-compose logs -f nginx`
